using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Todoer.Models;




namespace Todoer.Pages_Todoes
{
    public class ActiveModel : PageModel
    {
        private readonly TodoerDbContext _context;

        public ActiveModel(TodoerDbContext context)
        {
            _context = context;
        }

        public IList<Todo> Todo { get;set; } = default!;
        public List<Todo> NonActiveTodo { get; set; } = default!;

        public async Task OnGetAsync()
        {
            if (_context.Todo != null)
            {
                Todo = await _context.Todo.ToListAsync();
                NonActiveTodo = Todo.Where(x => x.IsDone == true).ToList();
            }
         }

          
    }
}
