namespace Todoer.Models
{
    public class Todo
    {
        public int Id { get; set; } = default!;
        public string Title { get; set; } = default !;
        public string Description { get; set; } = default !;
        public bool IsDone { get; set; } = default !;
        public DateTime Date{get;set;} = default!;
    }
} 